-*-----------------------------------------------------------------------------------------------
Macaulay2 codes for augmented conormal fans

Authors: Christopher Eur
Created: 10/16/2020
Comments:
------------------------------------------------------------------------------------------------*-

needsPackage "Matroids"


-------------------------------------< auxiliary functions >-------------------------------------
--elementary symmetric functions
elemSym = (i,L) -> if i == 0 then 1_(ring first L) else sum(subsets(L,i), j -> product j);

--multinomial coefficient
multinomial = (n,L) -> sub(n! / product(L, l -> l!),ZZ);


--input: a list of elements in a common ring
--output: a list of their elementary symmetrics
rToC = method()
rToC(List) := List => L -> if L === {} then {1} else apply(#L+1, i -> elemSym(i,L))


--input: a matroid M on ground set [n]
--output: a matroid M+e which is its free extension (the ground set is E \cup \{e\})
freeExt = method()
freeExt(Matroid) := Matroid => M -> (
    E := elements M.groundSet | {symbol e};
    B := ((bases M)/elements) | ((independentSets(M,rank M - 1))/(s -> (elements s) | {e}));
    matroid(E,B)
    )

--the free coextension of a matroid
freeCoext = method()
freeCoext(Matroid) := Matroid => M -> dual(freeExt(dual M))

--input: a m x n matrix A
--output: a m x n array in Julia format (e.g. [a b c ; d e f] is a 2 x 3 array)
toArray = method()
toArray(Matrix) := A -> (
    L := entries A;
    "[" | concatenate apply(L, r -> concatenate apply(r, i -> " " | (toString i)) | " ;") | "]"
    )



-----------------------------------< Schubert matroids >------------------------------------

--assuming that two lists A and B of size length are sorted, tests whether A \leq B in Gale order
galeLeq = (A,B) -> all(#A, i -> A_i <= B_i);

--given integer n and a list L, a sublist of {0,1,..,n-1}, outputs the corresponding Schubert
--matroid with L being the minimal basis w/r/t to Gale order
schubertMatroid = method()
schubertMatroid(ZZ,List) := Matroid => (n,L) -> (
    EL := apply(n, i -> i);
    r := #L;
    BL := select(subsets(EL,r)/sort, l -> galeLeq(L,l));
    matroid(EL, BL)
    )

--given a list that is a permutation of {0,1,..,n-1}, outputs a FunctionClosure that
--performs the permutation on a sublist
listToFunction = L -> l -> (
    apply(l, i -> L_i)
    )

--given a list L, erases repeated elemented where equality is tested by "==" instead of "==="
weakUnique = L -> (
    newL := {};
    apply(L, i -> if all(newL, j -> i != j) then newL = newL | {i});
    newL
    )

--all Schubert matroids (with S_n-symmetry allowed) of rank r on n elements
--the output is a list of lists, each list being an isomorphism class
allSchubertMatroids = method()
allSchubertMatroids(ZZ,ZZ) := List => memoize ((r,n) -> (
    ML := apply(subsets(n,r), l -> schubertMatroid(n,l));
    perms := (permutations n)/listToFunction;
    apply(ML, m -> weakUnique apply(perms, f -> matroid(m_*, sort((bases m)/elements/f))))
    ))

--computes the Poincare pairing matrix and its inverse
poincarePairing = method()
poincarePairing(ZZ,ZZ) := List => memoize ((r,n) -> (
    ML := flatten allSchubertMatroids(r,n);
    P := matrix apply(ML, i -> apply(ML/dual, j -> (
		Bi := bases i;
		Bj := bases j;
		if any((Bi ** Bj)/product, s -> s === set {}) then 1/1 else 0
		)
	    )
	);
    {P, inverse P}
    ))

--input: rank r, number of elements n, and a function f on matroids of rank r on n elements
--whose values live in a ring R.  Warning: sub(f(Matroid),R) needs to work
--Invariant => true will display the values for only the representative Schubert matroid with nonzero coeff
--output: list of two matrices; one which is values of f on all Schubert matroids of rank r on [n],
--and another which is the values their dual matroids should get to realize f as a Poincare pairing
valPairing = method(Options => {Invariant => false})
valPairing(ZZ,ZZ,Function,Ring) := opts -> (r,n,f,R) -> (
    preML := allSchubertMatroids(r,n);
    orbitSizes := preML/(i -> #i);
    firsts := {0};
    apply(#orbitSizes-1, i -> firsts = firsts | {(last firsts) + orbitSizes_i});
    ML := flatten preML;
    invP := last poincarePairing(r,n);
    FMat :=  transpose matrix {apply(ML, m -> sub(f(m),R))};
    ValMat := sub(invP,R) * FMat;
    if not opts.Invariant then return {FMat, ValMat};
    inds := select(positions(flatten entries ValMat, i -> i != 0), j -> member(j,firsts));
    apply(inds, i -> {ML_i, ValMat_(i,0)})
    )


-----------------------------------------< Chow Ring >----------------------------------------

--CAUTION: to avoid massive print out, initialize the Chow ring of M by, for e.g., AM = chowRing M

--input: a (loopless) matroid M
--output: the Chow ring of M in the presentation of Feichtner-Yuzvinsky
--with the Groebner basis entered in manually
chowRing = method()
chowRing(Matroid) := Ring => M -> (
    if not loops M == {} then error "only implemented for loopless matroids";
    if not M.cache.?chowRing then (
	L := dropElements(latticeOfFlats M, {{}});
	F := sort(L.GroundSet, i -> rank(M,set i));
	z := symbol z;
	R := QQ[F/(f -> z_f), MonomialOrder => Lex];
	var := hashTable apply(gens R, i -> (last baseName i, i));
	incomps := select(subsets(F,2), s -> #unique(s_0 | s_1) > max(#s_0, #s_1));
	quadRels := matrix {incomps/(s -> (var#(s_0))* (var#(s_1)))};
	comps := matrix {apply(chains(L,2), l -> (
	    f1 := first l; f2 := last l;
	    (var#f1)*(sum((filter(L,{f2}))/(f -> var#f)))^(rank(M,set f2) - rank(M,set f1))
	    )
	)};
	powers := matrix { apply(F, i -> (sum((filter(L,{i}))/(f -> var#f)))^(rank(M, set i))) };
	GBmatrix := (quadRels | comps | powers);
	forceGB GBmatrix;
	I := ideal GBmatrix;
	A := R/I;
	H := hashTable apply(gens ambient A, i -> (set last baseName i, sub(i,A)));
	M.cache.chowRing = R/I;
	M.cache.var = H;
	M.cache.nonemptyPoset = L;
	);
    M.cache.chowRing
)


--input: a matroid M
--output: a hashTable where keys are nonempty flats of M
--(as sets bcuz M2 doesn't know {0,1} and {1,0} are same flats)
--whose values are the corresponding variables in chowRing(M)
var = method()
var(Matroid) := HashTable => M -> (
    if not M.cache.?var then (
	A := chowRing M;
    	);
    M.cache.var
    )


--input: a matroid M on a ground set E
--output: the degree map on the chowRingFY(M), evaluates the (-z_E)^(rank M -1) to 1
deg = method();
deg(Matroid) := FunctionClosure => M -> (
    if not M.cache.?deg then (
    	d := rank M -1;
    	A := chowRing M;
    	lastVar := (var M)#(M.groundSet);
    	pt := sub(lastVar^d,A);
    	degM := a -> (
	    if a === 0_A then return 0/1;
	    if not ring a === A then error "not an element of Chow ring";
	    if not degree a == {d} then error "not a top degree element";
	    (-1)^d*sub(coefficient(pt,a),QQ)
	    );
	M.cache.deg = degM;
    	);
    M.cache.deg
    )


--input: matroids M and N forming a quotient M <<-- N
--output: the natural map A(M) <-- A(N) of their chow rings
chowPullback = method()
chowPullback(Matroid,Matroid) := (M,N) -> (
    if not isSubset(flats M, flats N) then error "first matroid need be quotient of second";
    AM := chowRing M;
    AN := chowRing N;
    varM := var M;
    varN := var N;
    map(AM,AN,apply(gens ambient AN, i -> (
		s := set last baseName i;
		if member(s, keys varM) then varM#s
		else 0_AM
		)
	    )
	)
    )

--alpha and beta classes in the Chow ring
--(alpha is the hyperplane class pullback from blow-ups of the linear space)
alpha = method()
alpha(Matroid) := RingElement => M -> -(var M)#(M.groundSet)

beta = method()
beta(Matroid) := RingElement => M -> sum(keys var M, k -> (var M)#k)


--input: a matroid M
--output: a function whose input is an element of chowRing(M) and output is its degree
--after multiplying the element by appropriate power of the alpha class
alphaDeg = method()
alphaDeg(Matroid) := FunctionClosure => M -> (
    degM := deg M;
    varM := var M;
    alpha := - varM#(M.groundSet);
    if not M.cache.?alphaDeg then (
	alphaDeg := a -> (
	    if instance(a,ZZ) or instance(a,QQ) then return a;
	    aTerms := terms a;
	    sum(aTerms, f -> (
		    if (degree f) === {} then sub(f,QQ)
		    else degM( alpha^(rank M - 1 - first degree f) * f)
		    )
		)
	    );
	M.cache.alphaDeg = alphaDeg;
	);
    M.cache.alphaDeg
    )

--input: a matroid M and a Boolean matroid U on its ground set
--output: the O(1) class of M (i.e. the divisor of the matroid polytope) in the Chow ring of U
O1Class = method()
O1Class(Matroid,Matroid) := RingElement => (M,U) -> (
    if M.groundSet =!= U.groundSet then error "check ground sets of matroids";
    varU := var U;
    sum(keys varU, i -> rank(M,i)*varU#i)
    )

------------------------------< Simplicial generated Chow ring >--------------------------------


--input: a matroid M
--output: Chow ring of the matroid M with presentation via variables h_F
--the groebner basis is hard-coded in, so the ideal presentation looks more ugly but is faster
chowRingH = method();
chowRingH(Matroid) := Ring => M -> (
    if not loops M == {} then << "only implemented for loopless matroids" << return error;
    if not M.cache.?chowRingH then (
	L := dropElements(latticeOfFlats M,{{}});
	moeb := moebiusFunction L;
	F := sort(L.GroundSet, f -> rank(M,set f));
    	h := symbol h;
	R := QQ[F/(f -> h_f), MonomialOrder => Lex];
    	var := hashTable apply(gens R, r -> (last baseName r, r));
    	incompPairs := select(subsets(L.GroundSet,2), s -> #unique(s_0 | s_1) > max(#s_0, #s_1));
	incomps := matrix {apply(incompPairs, l -> (
	    f1 := first l; f2 := last l;
	    sum(filter(L,{f1})/(g -> moeb#(f1,g)*var#g))*sum(filter(L,{f2})/(g -> moeb#(f2,g)*var#g))
	    )
	)};
	comps := matrix {apply(chains(L,2), l -> (
	    f1 := first l; f2 := last l;
	    sum(filter(L,{f1})/(g -> moeb#(f1,g)*var#g))*(var#f2)^(rank(M,set f2)-rank(M,set f1))
	    )
	)};
	powers := matrix {F/(f -> (var#f)^(rank(M,set f)) )};
	GBmatrix := (incomps | comps | powers);
	forceGB GBmatrix;
	I := ideal GBmatrix;
    	M.cache.chowRingH = R/I;
    );
    M.cache.chowRingH
)



----------------------------------< Augmented Chow ring >----------------------------------

--input: a matroid M that may have loops now
--output: the augmented Chow ring of M as presented in the semi-small paper but with z_E included
--(so that z_E = minus of alpha class)
augChowRing = method()
augChowRing(Matroid) := Ring => M -> (
    if not M.cache.?augChowRing then (
	E := M.groundSet;
	L := latticeOfFlats M;
	nonloops := E - set loops M;
	F := sort(L.GroundSet, i -> rank(M,set i));
	z := symbol z; y:= symbol y;
	R := QQ[(elements nonloops)/(i -> y_i), F/(f -> z_f), MonomialOrder => Lex];
	var := hashTable apply(gens R, i -> (last baseName i, i));
	Zincomps := select(subsets(F,2), s -> #unique(s_0 | s_1) > max(#s_0, #s_1));
	ZYincomps := flatten apply(F, f -> (elements (nonloops - set f))/(i -> {f,i}));
	quadRels := ideal apply(Zincomps | ZYincomps, s -> (var#(s_0))* (var#(s_1)));
	linearRels := ideal(sum(F, f -> var#f)) + ideal apply(elements nonloops, i -> (
		var#i + sum(select(F, f -> member(i,f)), g -> var#g)
		)
	    );
	A := R/(quadRels + linearRels);
	H := hashTable apply(gens ambient A, i -> (
		bn := last baseName i;
		(if instance(bn,List) then set bn else bn, sub(i,A))
		)
	    );
	M.cache.augChowRing = A;
	M.cache.augVar = H;
	M.cache.nonemptyPoset = L;
	);
    M.cache.augChowRing
    )


--input: a matroid M
--output: a hash table whose keys are indices of the variables in the augmented Chow ring of M
--and the values are corresponding variables (an index is either ZZ or Set).
augVar = method()
augVar(Matroid) := HashTable => M -> (
    if not M.cache.?augVar then (
	augChowRing M;
    	);
    M.cache.augVar
    )

--input: a matroid M
--output: a function which is the degree map on the augChowRing(M)
augDeg = method()
augDeg(Matroid) := FunctionClosure => M -> (
    if not M.cache.?augDeg then (
    	r := rank M;
    	CH := augChowRing M;
    	lastVar := (augVar M)#(M.groundSet);
    	pt := sub(lastVar^r,CH);
    	augDegM := a -> (
	    if a === 0_CH then return 0/1;
	    if not ring a === CH then error "not an element of Chow ring";
	    if not degree a == {r} then << "not a top degree element" << return 0;
	    (-1)^r*sub(coefficient(pt,a),QQ)
	    );
	M.cache.augDeg = augDegM;
    	);
    M.cache.augDeg
    )

--input: a matroid M and a boolean matroid U with same ground sets
--output: the divisor in augChowRing(U) corresponding to the independent polytope of M
divisor = method()
divisor(Matroid,Matroid) := RingElement => (M,U) -> (
    E := U.groundSet;
    if M.groundSet =!= E then error "ground sets not same";
    augVarU := augVar U;
    sum(subsets E, s -> rank(M,s) * augVarU#(E-s))
    )




--------------------------------< Higgs factorizations >----------------------------------


--input: matroids M and N where M is a quotient of N
--output: the Higgs lift HL_N(M) of M towards N
higgsLift = method();
higgsLift(Matroid,Matroid) := Matroid => (M,N) -> (
    E := M.groundSet; r := rank M;
    if not #E == #N.groundSet then << "ground sets are different" << return error;
    if not r < rank N then << "rank of first need be smaller than first" << return error;
    B := select(subsets(E,r+1), s -> rank(M,s) == r and rank(N,s) == r+1);
    matroid(toList E,B/toList)
)

--input: a matroid M
--output: a list of matroids {M=M^c,...,M^1,M^0=Boolean} representing the Higgs factorization of M
higgsFactorization = method();
higgsFactorization(Matroid) := List => M -> (
    n := #M.groundSet;
    r := rank M;
    U := uniformMatroid(n,n);
    L := {M};
    apply(n-r, i -> L = L | {higgsLift(last L,U)});
    L
)

--input: matroids M and N such that M <<-- N is a matroid quotient
--output: the Higgs factorization of (M,N)
higgsFactorization(Matroid,Matroid) := List => (M,N) -> (
    if not (M.groundSet === N.groundSet and isSubset(flats M, flats N)) then
    error " the first argument need be matroid quotient of the second ";
    r1 := rank M;
    r2 := rank N;
    E := M.groundSet;
    L := apply(r2 - r1 - 1, i -> (
	    I2 := set independentSets(N,r1+i+1);
	    I1 := set ((independentSets(dual M, #(elements E) - r1 - i - 1))/(s -> (E - s)));
	    B := (elements (I1 * I2))/elements;
	    matroid(elements E, B)
	    )
	);
    {M} | L | {N}
    )


--input: matroids M and N such that M <<-- N
--output: the full flag matroid with |E|+1 constituents (first and last are Unif(0,E), Unif(E,E))
--that is the Higgs lift of M,N
fullHiggsLift = method()
fullHiggsLift(Matroid,Matroid) := List => (M,N) -> (
    if not (M.groundSet === N.groundSet and isSubset(flats M, flats N)) then
    error " the first argument need be matroid quotient of the second ";
    n := #(elements M.groundSet);
    U0 := uniformMatroid(0,n);
    Un := uniformMatroid(n,n);
    unique (higgsFactorization(U0,M) | higgsFactorization(M,N) | higgsFactorization(N,Un))
    )

--input: matroid M
--output: the full flag matroid with |E|+1 constituents (first and last are Unif(0,E), Unif(E,E))
--that is the Higgs lift of M
fullHiggsLift(Matroid) := List => M -> fullHiggsLift(M,M)




-------------------------------< Cherns roots of S & Q >---------------------------------

--input: M a (loopless) matroid on ground set E, and U is the Boolean matroid on E
--output: a list of the Cherns roots of the tautological subbundle S on the Grassmannian
--pullbacked to the permutohedral variety via X_(A_n) --> X_Q(M) --> Grassmannian
SChernRoots = method()
SChernRoots(Matroid,Matroid) := List => (M,U) -> (
    E := U.groundSet;
    if M.groundSet =!= E then error "check ground sets of matroids";
    varU := var U;
    H := hashTable((p,q) -> p | q, apply(subsets E, i -> (rank(M,i), {i})));
    r := rank M;
    apply(rank M, i -> sum(toList((i+1)..r), j -> sum(H#j, k -> - varU#k)))
    )


--input: same as SChernRoots
--output: Chern roots of the tautological quotient bundle Q now
QChernRoots = method()
QChernRoots(Matroid,Matroid) := List => (M,U) -> (
    E := U.groundSet;
    if M.groundSet =!= E then error "check ground sets of matroids";
    varU := var U;
    hPairs :=  apply(delete(set {}, subsets E), i -> (#(elements i) - rank(M,i), {i}));
    H := hashTable((p,q) -> p | q, hPairs);
    c := #(elements E) - (rank M);
    apply(c, i -> sum(toList((c-i)..c), j -> sum(H#j, k -> - varU#k)))
    )


--input: a list of elements in a common ring
--output: a list of their elementary symmetrics
rToC = method()
rToC(List) := List => L -> if L === {} then {1} else apply(#L+1, i -> elemSym(i,L))

--input: a list L of elements in a common ring represent Chern roots, and a positive integer n
--output: Chern roots of the n-th wedge power
wedge = method()
wedge(ZZ,List) := List => (n,L) -> apply(subsets(L,n), l -> sum l)


--input: a loopless matroid M, and Boolean matroid U on ground set of M
--output: a matrix Mat whose (i,j)th entry is the (alpha) degree of c_i(Q|M) * c_j(Sdual|M)
chernDegs = method()
chernDegs(Matroid,Matroid) := (M,U) -> (
    if M.groundSet =!= U.groundSet then error "check ground sets of matroids";
    cSdual := rToC(-SChernRoots(M,U));
    cQ := rToC(QChernRoots(M,U));
    Mat := transpose matrix {cQ} * matrix {cSdual};
    matrix apply(entries Mat, r -> r/(i -> (alphaDeg U)i))
    )

--input: a loopless matroid M
--output: a matrix Mat whose (i,j)th entry is the (alpha) degree of c_i(Q|M) * c_j(Sdual|M)
chernDegs(Matroid) := M -> (
    n := #(elements M.groundSet);
    U := uniformMatroid(n,n);
    chernDegs(M,U)
    )

--augmented S and Q Chern roots
--given a matroid M and a Boolean matroid U on its ground set
--outputs two lists L1 = chern roots of S_M and L2 = chern roots of Q_M
augRoots = method()
augRoots(Matroid,Matroid) := List => (M,U) -> (
    E := U.groundSet;
    if M.groundSet =!= E then error "check ground sets of matroids";
    n := #(elements E);
    r := rank M;
    DivList := (fullHiggsLift dual M)/(m -> divisor(m,U));
    L1 := apply(r, i -> DivList_(n-r+i+1) - DivList_(n-r+i));
    L2 := apply(n - r, i -> DivList_(i+1) - DivList_i);
    {L1, L2}
    )

--output Segre classes given a list (the complete homogeneous symmetric polynoms)
--needs to give the ambient dimension n of the variety in which the Segre classes live
rToS = (L,n) -> (
    r := #L;
    R := ring first L;
    x := symbol x;
    S := QQ[x_1..x_r];
    homogs := apply(n+1, i -> (-1)^i * sum flatten entries basis(i,S));
    phi := map(R,S,L);
    homogs/phi
    )

end

------------------------------------------------------------
restart
load "augmentedBergman.m2"


n = 6
M = matroid completeGraph (n-1)
A = chowRingH M;
varsA = hashTable apply(gens ambient A, i -> (set last baseName i, sub(i,A)))
connflats = select(flats M, f -> #components(M|f) == 1)
minvarsA = hashTable apply(connflats, f -> (sum(M_(elements f)), varsA#f))
R = A[apply(keys minvarsA, i -> x_(toSequence(sort elements i)))]
l = sum(gens R, i -> (-1)^(#last baseName i - 1)*minvarsA#(set last baseName i) * i)
vp = factor l^(n-3);
value first vp
VP = value last vp

sub(VP, (gens R)/(i -> i => 1))

VP3 = sum select(terms VP, i -> (sort unique first exponents i) == {0,3})
VP3neg = sum select(terms VP3, i -> sub((last coefficients i)_(0,0),QQ) < 0 )

VP21 = sum select(terms VP, i -> (sort unique first exponents i) == {0,1,2})
VP21neg = sum select(terms VP21, i -> sub((last coefficients i)_(0,0),QQ) < 0 )
sub(VP21, (gens R)/(i -> i => 1))
sub(VP21neg, (gens R)/(i -> i => 1))


VP111 = sum select(terms VP, i -> all(first exponents i, j -> j < 2))
VP111neg = sum select(terms VP, i -> all(first exponents i, j -> j < 2) and sub((last coefficients i)_(0,0),QQ) < 0 )
sub(VP111, (gens R)/(i -> i => 1))
sub(VP111neg, (gens R)/(i -> i => 1))

VP - (VP3 + VP21 + VP111)
