--a sanity check
ML = select(allMatroids 6, m -> rank m == 1);
M = first random ML
sum(independentSets M, i -> (-1)^(rank M - #(elements i)) * tutteEvaluate(M/i,0,1))

-----------< Chern and Segree classes of QM intersections >-------------
restart
load "augmentedBergman.m2"

n = 5;
U = uniformMatroid(n,n);
E = U.groundSet;
time CH = augChowRing U;
alph = -(augVar U)#E;
degU = augDeg U;
O1s = apply(n, i -> (augVar U)#i);

degR = f -> (
    coeff := coefficients f;
    coeffZ := matrix ((entries last coeff)/(l -> {degU sub(first l,CH)}));
    first flatten entries ((first coeff) * coeffZ)
    );

M = uniformMatroid(2,n)
M = uniformMatroid(1,1) ++ uniformMatroid(1,n-1)
ML = select(allMatroids n, m -> rank m == 2)
M = first random ML

QM = last augRoots(M,U);
cQM = (rToC QM) | toList(rank M:0_CH);
sQMdual = rToS(-QM,n);
f = sum(n+1, i -> cQM_i * v^i) * sum(n+1, i -> sQMdual_i * u^i) * product(n, i -> (1+O1s_i * w_i));
degR f
sub(oo, apply(n, i -> w_i => 0))

R = CH[u,v,w]; S = frac(QQ[u,v,w]);
f = sum(n+1, i -> cQM_i * v^i) * sum(n+1, i -> sQMdual_i * u^i) * sum(n+1, i -> alph^i*w^i);
sub(degR f,S) == (v+w)^(n - rank(M))* u^(rank M) * tutteEvaluate(M,w/u,(u+v+w)/(v+w))

testing = M -> (
    QM := last augRoots(M,U);
    cQM := (rToC QM) | toList(rank M:0_CH);
    sQMdual := rToS(-QM,n);
    R = CH[u,v,w];
    S = frac(QQ[u,v,w]);
    f = sum(n+1, i -> cQM_i * R_1^i) * sum(n+1, i -> sQMdual_i * R_0^i) * sum(n+1, i -> alph^i*R_2^i);
    sub(degR f,S) == (v+w)^(n - rank(M))* u^(rank M) * tutteEvaluate(M,w/u,(u+v+w)/(v+w))
    )

ML/testing


-----------< QM and QMperp intersections >-------------
restart
load "augmentedBergman.m2"

n = 4;
U = uniformMatroid(n,n);
E = U.groundSet;
time CH = augChowRing U;
alph = -(augVar U)#E;
degU = augDeg U;
O1s = apply(n, i -> (augVar U)#i);
degR = f -> (
    coeff := coefficients f;
    coeffZ := matrix ((entries last coeff)/(l -> {degU sub(first l,CH)}));
    first flatten entries ((first coeff) * coeffZ)
    );

M = uniformMatroid(2,n)
M = uniformMatroid(1,1) ++ uniformMatroid(1,n-1)
ML = select(allMatroids n, m -> rank m == 2)
M = first random ML

(QM, QMperp) = (last augRoots(M,U), last augRoots(dual M,U))
cQM = (rToC QM) | toList(rank M:0_CH);
cQMperp = (rToC QMperp) | toList(rank dual M : 0_CH);
R = CH[u,v,w_0..w_(n-1)]
f = sum(n+1, i -> cQM_i * u^i) * sum(n+1, i -> cQMperp_i * v^i) * product(n, i -> (1+O1s_i * w_i));
degR f
sub(oo, apply(n, i -> w_i => 1))
degR (sum(n+1, i -> cQM_i * u^i) * sum(n+1, i -> cQMperp_i * v^i) * sum(n+1, i -> alph^i*w_1^i))




----------<gamma quick test>------------
restart
load "augmentedBergman.m2"


fVector M
gamma = m -> (
    F := hashTable apply(rank m + 1, i -> (i,#(independentSets(m,i))));
    (-1)^(rank m) * sum(keys F, k -> (-1)^k * k! * F#k)
    )
gamma uniformMatroid(5,5)
gamma uniformMatroid(2,3)

n = 7
ML = select(allMatroids n, m -> rank m == 4);
ML/gamma
select(ML, m -> gamma m == 0)
netList (oo/bases)


n = 7
m = 6
sum(m, i -> 1/((n-i)!))
1/((n-m)!)

-----------< augmented tautological Segre classes intersections >-------------
restart
load "augmentedBergman.m2"

S = frac(QQ[x,y]);
n = 5;
U = uniformMatroid(n,n);
E = U.groundSet;
time CH = augChowRing U;
alph = -(augVar U)#E;
degU = augDeg U;
O1s = apply(n, i -> (augVar U)#i);

M = uniformMatroid(3,n)
ML = allMatroids n
M = first random ML
(SM, QM) = toSequence augRoots(M,U)
sQMdual = rToS(-QM,n)
apply(n+1, j -> degU( elemSym(n-j,O1s) * sQMdual_j ))
apply(subsets n, l -> degU( product(O1s_l) * sQMdual_(n-#l)))
sSMshift = rToS(SM/(i -> i - sum O1s),n)
apply(n+1, j -> degU( elemSym(n-j,O1s) * sSMshift_j ))
hashTable apply(subsets n, l -> (l,degU( product(O1s_l) * sSMshift_(n-#l))))

cSM = (rToC SM) | toList(n-rank M:0_CH);
cQM = (rToC QM) | toList(rank M : 0_CH);
matrix apply(n, i -> apply(n, j -> degU( elemSym(n-i-j,O1s) * cSM_i * cQM_j )))
tutteM = sum flatten apply(n, i -> apply(n, j -> (
	    degU( elemSym(n-i-j,O1s) * cSM_i * cQM_j) * S_0^i * S_1^(n-rank M-j)
	    )
	)
    )
tutteM == tutteEvaluate(M,x+1,y+1)



-----------< updated augmented tautological classes tests >-------------
restart
load "augmentedBergman.m2"

S = frac(QQ[x,y]);
n = 5;
U = uniformMatroid(n,n);
E = U.groundSet;
time CH = augChowRing U;
alph = -(augVar U)#E;
degU = augDeg U;
O1s = apply(n, i -> (augVar U)#i);

M = uniformMatroid(2,5)
ML = allMatroids 5
M = first random ML
(SM, QM) = toSequence augRoots(M,U)
cSM = (rToC SM) | toList(n-rank M:0_CH);
cQM = (rToC QM) | toList(rank M : 0_CH);
matrix apply(n, i -> apply(n, j -> degU( elemSym(n-i-j,O1s) * cSM_i * cQM_j )))
tutteM = sum flatten apply(n, i -> apply(n, j -> (
	    degU( elemSym(n-i-j,O1s) * cSM_i * cQM_j) * S_0^i * S_1^(n-rank M-j)
	    )
	)
    )
tutteM == tutteEvaluate(M,x+1,y+1)
cSMshiftDual = (rToC (-(SM/(i -> i - sum O1s)))) | toList (n-rank M:0_CH)
shiftCherns = sum flatten apply(n, i -> apply(n, j -> (
	    degU(cSMshiftDual_i * cQM_j) * S_0^i * S_1^j
	    )
	)
    )
(-1)^(rank M) * sum(rank M+1, i -> (-1)^i * i! * #(independentSets(M,i)))



-----------< alpha, O(1)'s, and chern of augmented S_L ( = Q_L^matt dual) intersections >----------

restart
load "augmentedBergman.m2"

S = QQ[x,y];
n = 5;
U = uniformMatroid(n,n)
E = U.groundSet;
time CH = augChowRing U;
basis(n,CH)
alph = -(augVar U)#E;
degU = augDeg U;
O1s = apply(n, i -> (augVar U)#i);
cS = m -> (
    DivList := (fullHiggsLift m)/(i -> divisor(i,U));
    RootList := apply(rank m, i -> DivList_(i+1) - DivList_i);
    rToC RootList
    )
cQ = m -> (
    DivList := (fullHiggsLift m)/(i -> divisor(i,U));
    RootList := apply(n - rank m, i -> DivList_(rank m+i+1) - DivList_(rank m +i));
    rToC RootList
    )

ML = allMatroids 5
M = first random ML
cSM = cS M | toList(n-rank M:0_CH);
cQM = cQ M | toList(rank M : 0_CH);
matrix apply(n, i -> apply(n, j -> degU( elemSym(n-i-j,O1s) * cSM_i * cQM_j )))
tutteEvaluate(M,x+1,y+1)
L = {1,2}
matrix apply(n, i -> apply(n, j -> degU( product(L, i -> O1s_i) * cSM_i * cQM_j )))

--some concrete matroids
M = uniformMatroid(1,1)++uniformMatroid(2,4)
M = uniformMatroid(3,5)
M = matroid graph{{a,b},{b,c},{c,d},{d,a},{a,c}}


D = divisor(uniformMatroid(n-1,n),U)
apply(rank M + 1, i -> degU( D^(n-i) * cSM_i ))
apply(rank M + 1, i -> degU( elemSym(n-i,O1s) * cSM_i))

R = QQ[w_0..w_n]
--should equal the spanning subset generating function of the DUAL matroid
sum(flatten entries basis(n,R), m -> (
	expo := first exponents m;
	coeff := degU(cSM_(expo_0) * product(n, i -> (O1s_i)^(expo_(i+1))));
	coeff * m
	)
    )
--(alpha + O(1) giving Segre) to power?
sum(flatten entries basis(n,R), m -> (
	expo := first exponents m;
	mpart := sum(expo_0+1, i -> binomial(expo_0,i)*alph^i*cSM_(expo_0 - i));
	coeff := degU(mpart * product(n, i -> (O1s_i)^(expo_(i+1))));
	coeff * m
	)
    )



--alpha degrees are all binom coeff no matter what the matroid is
test = m -> (
    cSm := cS m;
    apply(rank m + 1, i -> ( alph^(n-i) * cSm_i ))
    )
ML = select(allMatroids 5, m -> rank m == 3)
ML/test


---------------< trying to represent valuations by Poincare pairing >------------------

--relation to the s^\leq valuations in Derksen-Fink
restart
load "augmentedBergman.m2"

time ML = flatten allSchubertMatroids(1,3);

member(Matroid,Matroid) := ZZ => (M,N) -> (
    if isSubset(bases M, bases N) then 1 else 0
    )

ML = allSchubertMatroids(2,4)
V = valPairing(2,4, m -> member(m,(ML/first)_4),ZZ);
select(flatten entries last V, v -> v != 0)
positions(flatten entries last V, v -> v != 0)
(flatten ML)_oo
oo/bases

--Tutte polynomials and some other interesting invariants
restart
load "augmentedBergman.m2"

time ML = flatten allSchubertMatroids(1,3);
poincarePairing(1,3)


S = QQ[symbol x, symbol y, Inverses => true, MonomialOrder => GLex]

valPairing(1,3, m -> tutteEvaluate(m,S_0+1,S_1+1),S)
valPairing(2,4, m -> tutteEvaluate(m,S_0+1,S_1+1),S)
VP = valPairing(3,6, m -> tutteEvaluate(m,S_0+1,S_1+1),S, Invariant => true);
netList apply(VP, l -> {first sort ((bases first l)/elements/sort), last l})


invertedTutte = m -> (S_0)^(rank m)*(S_1)^(#elements m.groundSet - rank m)*tutteEvaluate(m,S_0^(-1),S_1^(-1))

VP = valPairing(2,5, m -> tutteEvaluate(m,1,0) ,S, Invariant => true);
netList apply(VP, l -> {first sort ((bases first l)/elements/sort), last l})


betaInvar = m -> (
    f := sub(tutteEvaluate(m,S_0,0) * S_0^(-1),S);
    sub(f, S_0 => 0)
    )

n = 5, r = 2
VP = valPairing(r,n,betaInvar, S, Invariant=>true);
netList apply(VP, l -> {first sort ((bases first l)/elements/sort), last l})


needsPackage "Polyhedra"

ehrhart(Matroid) := RingElement => M -> ehrhart convexHull basisIndicatorMatrix M;

valPairing(2,4,ehrhart)
valPairing(2,5,ehrhart)

-*-- sanity checks
bases schubertMatroid(4,{0,1})
bases schubertMatroid(4,{0,2})
bases schubertMatroid(4,{1,2})
bases schubertMatroid(4,{0,3})
--*-



------< (Non?)-positivity for intersection of Schubert augmented Bergman classes >------
restart
load "augmentedBergman.m2"

n = 5
U = uniformMatroid(n,n)
time A = chowRingH U; --the Chow ring of (n-1)-dimensional permutohedral variety
varH = hashTable apply(gens ambient A, i -> (set last baseName i, sub(i,A)));
apply(n, i -> #flatten entries basis(i,A))
l = sum(select(subsets(n,3), i -> member(0,i))/set, i -> varH#i)
l * l 

---------------------< Julia-ready string output for U_2,3 >---------------------
restart
load "augmentedBergman.m2"

U = uniformMatroid(2,3)
V = dual U
M = U++V
M_*
N = freeCoext M
HL = ((fullHiggsLift N)/basisIndicatorMatrix/(a -> a^{0,1,2,3,4,5}))_{4,5,6}

Q4 = "Q4 = " | toArray HL_0
Q5 = "Q5 = " | toArray HL_1
Q6 = "Q6 = " | toArray HL_2

S1 = toArray transpose matrix {{0,0,0,0,0,0},{1,0,0,0,0,0}}--h^(1)_1
S2 = toArray transpose matrix {{0,0,0,0,0,0},{0,1,0,0,0,0}}--h^(1)_1
S3 = toArray transpose matrix {{0,0,0,0,0,0},{0,0,1,0,0,0}}--h^(1)_1

needsPackage "Polyhedra"
S1 = convexHull transpose matrix {{0,0,0,0,0,0},{1,0,0,1,0,0}}--h^(1)_1
S2 = convexHull transpose matrix {{0,0,0,0,0,0},{0,1,0,0,1,0}}--h^(1)_1
S3 = convexHull transpose matrix {{0,0,0,0,0,0},{0,0,1,0,0,1}}--h^(1)_1
P = S1 + S2 + S3
toArray transpose matrix ((latticePoints P)/entries/flatten)

---------------------< alpha1, alpha2, delta intersection pattern >---------------------
restart
load "augmentedBergman.m2"

SB = M -> (
    n := #(elements M.groundSet);
    w := symbol w;
    R := QQ[w_0..w_n, MonomialOrder => GLex];
    sum((bases M)/elements, l -> product(l, i -> R_0+R_(i+1)))
    )

VP = M -> (
    n := #(elements M.groundSet);
    r := rank M;
    w := symbol w;
    R := QQ[w_0..w_n, MonomialOrder => GLex];
    f := sum(r+1, i -> n!/((r-i)!*(n-r)!) * (
	    sum(independentSets(M,i)/elements, l -> product(l, i -> R_(i+1)) * R_0^(n-i))
	    )
	);
    g := diff(R_0^(n-r),f)/n!;
    toDividedPowers g
    )

M = matroid graph {{a,b},{b,c},{c,d},{d,a},{a,c}}
M = uniformMatroid(3,6)
SB M
VP M


n = 5
r = 3
w = symbol w;
R = QQ[w_0..w_n, MonomialOrder => GLex];
B = sum(subsets(toList(1..n),r), s -> product(s, i -> R_0+R_i))
VP = sum(r+1, i -> n!/((r-i)!*(n-r)!) * sum(subsets(toList(1..n),i), s -> product(s, i -> R_i) * R_0^(n-i)))
diff(R_0^(n-r),VP)/n!
B

-----------------------< delta class computation for U_{1,2} >--------------------------
restart
load "augmentedBergman.m2"
needsPackage "Polyhedra"


--faster mixed volume via formula (3) on pg. 116 of Fulton Toric Variety
myMixedVolume = method()
myMixedVolume(List) := L -> (
    n := ambDim first L;
    myVolume := p -> if dim p < ambDim p then 0 else latticeVolume p;
    sum(drop(subsets L,1), l -> (-1)^(n-#l)*myVolume(sum l)) / n!
    )

--sanity checks for myMixedVolume
L1 = convexHull matrix{{0,1},{0,0}}, vertices L1
L2 = convexHull matrix{{0,0},{0,1}}, vertices L2
mixedVolume {L1,L2}
myMixedVolume {L1,L2}
P = crossPolytope 2
Q = hypercube 2
mixedVolume {P,Q}
myMixedVolume {P,Q}
P = crossPolytope 3
time latticeVolume P
--time mixedVolume {P,P,P} --does not terminate in a minute!! the heck? 
time myMixedVolume {P,P,P}
P = hypercube 3
time latticeVolume P
--time mixedVolume {P,P,P} --does not terminate again...
time myMixedVolume {P,P,P}

P = crossPolytope 4
V = transpose matrix ((latticePoints P)/entries/flatten/(l -> l + {1,1,1,1}))
toString entries V
S = simplex 5
time myMixedVolume {S,S,S,S,S}


--------computation for the U_{1,2} begins here------------

--the matroid which is the free-coext of U_{1,2} ++ U_{1,2}
M = matroid graph {{a,b},{b,c},{c,d},{d,a},{a,c}}
--bases of M and the first Higgs lift of M (the next lift is the Boolean matroid)
B = ((fullHiggsLift M)/basisIndicatorMatrix)_{3,4}

--the matroid polytopes but with one coordinate removed (i.e. the second row is removed)
Q1 = convexHull (first B)^{0,2,3,4}
Q2 = convexHull (last B)^{0,2,3,4}
vertices Q1, vertices Q2, B --checking

--sanity check for the normal fan of Q1 versus the original matroid polytope Q1'
Q1' = convexHull (first B)
F1 = normalFan Q1
F1' = normalFan Q1'
{F1,F1'}/rays
maxCones F1
maxCones F1'
latticeVolume Q1
latticeVolume Q1'

--sanity check for Q2 versus the original matroid polytope Q2' = Q(M)
Q2' = convexHull (last B)
rays normalFan Q2
rays normalFan Q2'
latticeVolume Q2
latticeVolume Q2'

--the following is the wrong delta class
--note the labelling of the elements in M (i.e. M_*)
S = convexHull transpose (transpose matrix 0_(ZZ^(4)) || (id_(ZZ^2) | id_(ZZ^2))), vertices S

--the true polytope of the delta class
trueS = convexHull matrix{{0,1,0},{0,1,0},{0,0,1},{0,0,1}}, vertices trueS --h^(3)_{1,2}

--the corrected computation; the difference is 2
myMixedVolume {Q2,Q1,trueS,trueS}
myMixedVolume {Q2,Q2,trueS,trueS}

--the wrong computation before
myMixedVolume {Q2,Q1,S,S}
myMixedVolume {Q2,Q2,S,S}


--P^1 pullbacks
S1 = convexHull matrix{{0,1},{0,1},{0,0},{0,0}}, vertices S1 --h^(3)_1
S2 = convexHull matrix{{0,0},{0,0},{0,1},{0,1}}, vertices S2 --h^(3)_2
H1 = convexHull matrix{{0,1},{0,0},{0,0},{0,0}}, vertices H1 --h^(1)_1
H2 = convexHull matrix{{0,0},{0,0},{1,0},{0,0}}, vertices H2 --h^(1)_2


2*myMixedVolume{Q2,Q2,S1,S2}
2*myMixedVolume{Q2,Q1,S1,S2}

myMixedVolume{Q2,Q1,H1,S2} - myMixedVolume{Q2,Q2,H1,S2}
myMixedVolume{Q2,Q1,H1,S1} - myMixedVolume{Q2,Q2,H1,S1}
myMixedVolume{Q2,Q1,H2,S2} - myMixedVolume{Q2,Q2,H2,S2}
myMixedVolume{Q2,Q1,H2,S1} - myMixedVolume{Q2,Q2,H2,S1}




-------------computation for U_{1,1}++U_{0,0}---------------
--the matroid which is the free-coext of U_{1,2} ++ U_{1,2}
M = uniformMatroid(2,2) ++ uniformMatroid(1,3)
--bases of M and the first Higgs lift of M (the next lift is the Boolean matroid)
B = ((fullHiggsLift M)/basisIndicatorMatrix)_{3,4}

--the matroid polytopes but with one coordinate removed (i.e. the second row is removed)
Q1 = convexHull (first B)^{0,1,2,3}
Q2 = convexHull (last B)^{0,1,2,3}
vertices Q1, vertices Q2, B --checking

S = convexHull matrix{{0,1,0},{0,0,1},{0,0,1},{0,1,0}}, vertices S

myMixedVolume {Q2,Q1,S,S}
myMixedVolume {Q2,Q2,S,S}
--the difference is 1



-------------computation for U_{1,1}++U_{1,2}---------------


--chow ring computed by hand
x = symbol x
R = QQ[x_0..x_6]
F = {{0,1,4},{0,1,5},{1,2,4},{1,2,5},{2,3,4},{0,3,4},{0,5,6},{0,3,6},{2,5,6},{2,3,6}}/set
E = set {0,1,2,3,4,5,6}
I = dual monomialIdeal apply(F, f -> product(elements (E-f), i -> R_i))
J = ideal(R_0 - R_2, R_1 - R_0 - R_3 - R_6, R_4 - R_5 - R_6)
A = R/(I+J) --chow ring of Bl_p P^2 x P^1 blown-up at E x pt
hilbertSeries(oo, Reduce=>true)
--O(1,1) pullback minues the exceptional has degree 2
sub((R_0+R_3+R_4)^3,A)
--compare with just the O(1,1) pullback, which has degree 3 = (3 choose 1)
sub((R_0+R_3+R_4+R_6)^3,A)
--the exceptional cubed is truly 1 (not -1)
sub(R_0*R_1*R_4,A)

--checking with the toric variety package
needsPackage "NormalToricVarieties"
X = hirzebruchSurface 1
rays X, max X
P = toricProjectiveSpace 1
rays P
Y = X ** P
rays Y 
B = toricBlowup({1,5},Y)
rays B, max B
intersectionRing B
hilbertSeries(oo, Reduce=>true)

-------------computation for U_{0,1}++U_{2,2}---------------
needsPackage "NormalToricVarieties"
P2 = toricProjectiveSpace 2
P1 = toricProjectiveSpace 1
X = P2 ** P1
rays X, max X
B = toricBlowup({2,4},X)
preA = intersectionRing B
R = ambient A
sub((R_0+R_3-R_5)^3,A)
sub(R_1*R_2*R_3,A)


----< computing delta more generally (under construction) >----
restart
load "augmentedBergman.m2"
needsPackage "NormalToricVarieties"

n = 4
P = convexHull (transpose matrix permutations {1,1,0,0})^(toList(1..3))
vertices P
dim P
peek (normalFan P).cache

S = convexHull transpose (transpose matrix 0_(ZZ^(2*n)) || (id_(ZZ^n) | id_(ZZ^n)))
vertices S
Q = P*P
time (Q + S)
time F = normalFan(P*P+S)
peek F.cache
rays F

latticeVolume(P*P+S +S)



-----------------------< Tutte polynomials and free coextensions? >--------------------------

restart
load "augmentedBergman.m2"

R = QQ[x,y]
n = 7
U = uniformMatroid(n,n)
AU = chowRing U

M = matroid graph{{a,b},{b,c},{c,d},{d,a},{a,c}}
M = uniformMatroid(3,6)
r = rank M
N = freeCoext M
O1s = drop((fullHiggsLift N)/(m -> O1Class(m,U)),-1);
RootList = apply(n-1, i -> O1s_(i+1) - O1s_i);
cS = rToC(RootList_(toList(0..(r-1))));
cQ = rToC(-RootList_(toList(r..(n-2))));
mat = transpose matrix{cS} * matrix{cQ};
degU = deg U;
alphaU = alpha U;
transpose matrix apply(entries mat, r -> apply(r, i -> (degU)(alphaU^(n -1 - first degree i) * i)))
tutteEvaluate(M,x+1,y+1)


-----------------------< augmented Chow ring tests >--------------------------

restart
load "augmentedBergman.m2"

--sanity check with n = 3
U = uniformMatroid(3,3)
time CH = augChowRing U
hilbertSeries(CH, Reduce => true)
basis(3,CH)
M = uniformMatroid(2,3)
(fullHiggsLift M)/(m -> divisor(m,U))/(D -> (augDeg U) D^3)


--betti numbers

AL = apply(toList(1..6), n -> time numerator hilbertSeries(augChowRing uniformMatroid(n,n),Reduce=>true))
L = apply(toList(2..7), n -> time numerator hilbertSeries(chowRing uniformMatroid(n,n),Reduce=>true))
netList AL
netList L

n = 6
U = uniformMatroid(n,n)
time CH = augChowRing U
UU = uniformMatroid(n+1,n+1)
time A = chowRing UU

ML = select(allMatroids n, m -> rank m == 4);
M = uniformMatroid(3,6)
M = first random ML
time (fullHiggsLift M)/(m -> divisor(m,U))/(D -> (augDeg U) D^n)
apply(n, i -> oo_(i+1) - oo_i)
time (drop(fullHiggsLift M,1))/(m -> O1Class(freeExt m,UU))/(D -> (deg UU) D^n)
--actually, this seeming coincidence is just due to the fact:
--IP(M) - IP(trunc(M)) \simeq P(M+e) as polytopes.

AL = apply(fullHiggsLift M, m -> time numerator hilbertSeries(augChowRing m,Reduce=>true))
L = apply(fullHiggsLift M, m -> time numerator hilbertSeries(chowRing freeCoext m,Reduce=>true))
netList AL
netList L



--experiment with n = 6
n = 6
U = uniformMatroid(n,n)
time CH = augChowRing U --9 seconds
hilbertSeries(CH, Reduce => true)
basis(n,CH)
M = matroid completeGraph 4
M = uniformMatroid(3,6)
r = rank M
DivList = (fullHiggsLift M)/(m -> divisor(m,U));
RootList = apply(n, i -> DivList_(i+1) - DivList_i);
cS = rToC(RootList_(toList(0..(r-1))));
cQ = rToC(RootList_(toList(r..(n-1))));
mat = transpose matrix{cS} * matrix{cQ};
augDegU = augDeg U;
augAlpha = -(augVar U)#(U.groundSet)
transpose matrix apply(entries mat, r -> apply(r, i -> (augDeg U)(augAlpha^(n - first degree i) * i)))
tuttePolynomial M


--experiment with n = 4
n = 4
U = uniformMatroid(n,n)
time CH = augChowRing U
hilbertSeries(CH, Reduce => true)
basis(n,CH)
M = uniformMatroid(3,4)
r = rank M
DivList = (fullHiggsLift M)/(m -> divisor(m,U))
RootList = apply(n, i -> DivList_(i+1) - DivList_i);
cS = rToC(RootList_(toList(0..(r-1))));
cQ = rToC(RootList_(toList(r..(n-1))));
mat = (transpose matrix{cQ}) * matrix{cS};
augDegU = augDeg U;
augAlpha = -(augVar U)#(U.groundSet);
matrix apply(entries mat, r -> apply(r, i -> (augDeg U)(augAlpha^(n - first degree i) * i)))
tuttePolynomial M



-----------------------< alpha beta intersection sanity check >--------------------------

restart
load "augmentedBergman.m2"

ML = select(allMatroids 7, m -> rank m == 4 and loops m =!= {})
M = first random ML
loops M
apply(rank M + 1 , i -> #independentSets(M,i))

N = freeCoext M
f = (characteristicPolynomial N)
f / ((ring f)_0 - 1)

A = chowRing N
degN = deg N
aa = alpha N
bb = beta N
apply(rank M + 1, i -> degN (aa^(rank M - i) * bb^i))




-----------------------< chi_M coefficients <= f-vector M >--------------------------

restart
needsPackage "Matroids"

R = QQ[symbol q];

chiVect = m -> flatten entries last coefficients tutteEvaluate(m, R_0 + 1, 0)
fVect = m -> flatten entries last coefficients tutteEvaluate(m, 1+R_0, 1)

U = uniformMatroid(2,4)
chiVect U
fVect U

test = m -> (
    c := chiVect m;
    f := fVect m;
    all(rank m, i -> c_i <= f_i)
    )


ML = select(allMatroids 5, m -> rank m > 1 and loops m === {});
M = first random ML
chiVect M, fVect M
test M

ML/test

S = QQ[symbol x, symbol y]
tutteVar = m -> (
    f := tutteEvaluate(m, x,y) - x * tutteEvaluate(m, 1,y);
    f/(x-1)
    )

M = first random ML
tutteVar M
