using MixedSubdivisions

#the matroid polytopes 
Q4 = [ 1 1 1 1 1 1 1 1 1 1 1 1 ; 1 1 0 0 1 1 1 1 1 0 0 0 ; 0 0 1 1 0 1 0 0 1 1 1 1 ; 0 0 0 0 1 0 0 1 0 1 0 1 ; 0 1 0 1 0 0 1 1 1 0 1 1 ; 1 0 1 0 1 1 1 0 0 1 1 0 ;]
Q5 = [ 1 1 1 1 1 1 1 1 1 1 1 1 1 ; 1 1 1 1 0 1 0 1 0 1 1 1 0 ; 1 0 0 1 1 1 1 0 1 1 1 0 1 ; 0 0 1 1 1 0 0 1 1 1 0 1 1 ; 0 1 1 0 1 1 1 0 1 1 1 1 0 ; 1 1 1 1 0 0 1 1 1 0 1 0 1 ;]
Q6 = [ 1 1 1 1 1 1 ; 1 1 1 1 1 0 ; 1 1 0 1 1 1 ; 1 1 1 0 1 1 ; 1 0 1 1 1 1 ; 1 1 1 1 0 1 ;]

#function for doing the mixed volume [Q6, Q5-Q6, Q4-Q5, x, y, z]
function MVB(x,y,z)
	return mixed_volume([Q6, Q5, Q4, x, y, z]) - mixed_volume([Q6, Q6, Q4, x, y, z]) - mixed_volume([Q6, Q5, Q5, x, y, z]) + mixed_volume([Q6, Q6, Q5, x, y, z])
end

#the h^(1)_i's
S1 = [ 0 1 ; 0 0 ; 0 0 ; 0 0 ; 0 0 ; 0 0]
S2 = [ 0 0 ; 0 1 ; 0 0 ; 0 0 ; 0 0 ; 0 0]
S3 = [ 0 0 ; 0 0 ; 0 1 ; 0 0 ; 0 0 ; 0 0]

#the h^(3)_i's
H1 = [ 0 1 ; 0 0 ; 0 0 ; 0 1 ; 0 0 ; 0 0]
H2 = [ 0 0 ; 0 1 ; 0 0 ; 0 0 ; 0 1 ; 0 0]
H3 = [ 0 0 ; 0 0 ; 0 1 ; 0 0 ; 0 0 ; 0 1]
#their sum
H = [ 0 1 0 1 1 0 0 1 ; 0 0 1 1 0 0 1 1 ; 0 1 0 0 0 1 1 1 ; 0 1 0 1 1 0 0 1 ; 0 0 1 1 0 0 1 1 ; 0 1 0 0 0 1 1 1]

w0to3 = MVB(H,H,H) #should be 3! * 2
w1w0to2 = MVB(S1,H,H) #should be 2! * 2
w2w0to2 = MVB(S2,H,H) #should be 2! * 2
w3w0to2 = MVB(S3,H,H) #should be 2! * 2
w1w2w0 = MVB(S1,S2,H) #should be 1! * 2
w1w3w0 = MVB(S1,S3,H) #should be 1! * 2
w2w3w0 = MVB(S2,S3,H) #should be 0
