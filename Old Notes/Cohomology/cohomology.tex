 \documentclass{amsart}
 \setlength{\textwidth}{\paperwidth}
\addtolength{\textwidth}{-2in}
\calclayout
 \usepackage{amssymb,amsmath,amsfonts,epsfig,latexsym,tikz, amsrefs}
 \usepackage{tikz-cd}
\usepackage{hyperref}
\usepackage{enumerate}
\usepackage{mathtools}
\usepackage{verbatim}
\usepackage{cleveref}
%\usepackage{caption}
\usepackage[shortlabels]{enumitem}
\usepackage{subcaption}

\usetikzlibrary{positioning}
\usetikzlibrary{matrix}
\usetikzlibrary{decorations}
\usetikzlibrary{decorations.pathreplacing, decorations.pathmorphing, angles,quotes}
 
 \newtheorem{theorem}{Theorem}[section]
\newtheorem{definition}[theorem]{Definition}
\newtheorem{proposition}[theorem]{Proposition}
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{corollary}[theorem]{Corollary}
\newtheorem{conjecture}[theorem]{Conjecture}
\newtheorem{claim}[theorem]{Claim}

\newcommand{\matt}[1]{{ \sf $\clubsuit\clubsuit\clubsuit$ {\textcolor{red}{Matt: [#1]}}}}

\theoremstyle{definition}
\newtheorem{remark}[theorem]{Remark}
\newtheorem{example}[theorem]{Example}
\newtheorem{notation}[theorem]{Notation}
\newtheorem{question}[theorem]{Question}
\begin{document}

\title{Stellahedral geometry of matroids}          
\author{Chris Eur}
\author{June Huh}
\author{Matt Larson}
\date{\today}
\address{Stanford U. Department of Mathematics, 450 Jane Stanford Way, Stanford, CA 94305}
\email{mwlarson@stanford.edu}
\begin{abstract}
\end{abstract}
 
\maketitle

\vspace{-20 pt}



\bibliography{sources}
\bibliographystyle{amsalpha}

\section{Cohomology of bundles}

First we recall Klyachko's classification of toric vector bundles in terms of linear algebraic data, and then we describe the augmented tautological bundles in these terms. Klyachko gives a complex that computes the cohomology of a toric vector bundle in terms of linear algebraic data, and we use that to compute the cohomology of $\mathcal{S}_M$ and $\mathcal{Q}_M$. Using the short exact sequence $0 \to \mathcal{O}_{X_{St_n}}(-X_{A_{n-1}}) \to \mathcal{O}_{X_{St_n}} \to \mathcal{O}_{X_{A_{n-1}}} \to 0$, we compute the cohomology of $\underline{\mathcal{S}}_M$ and $\underline{\mathcal{Q}}_M$. 

Let $X = X(\Delta)$ be a toric variety over a field $k$. For a cone $\sigma$ of $\Delta$, let $M_{\sigma} = M/(\sigma^{\perp} \cap M)$. For a ray $\rho$ of $\Delta$, let $v_{\rho}$ denote the primitive vector on $\rho$. 

\begin{theorem}\label{thm:vectbundle}\cite{Klaychko}*{Theorem 0.1.1}
The category of toric vector bundle on $X(\Delta)$ is equivalent to the category of finite dimensional $k$-vector space $E$ equipped with collections of decreasing filtrations $\{E^{\rho}(i)\}_{i \in \mathbb{Z}, \rho \in \Delta(1)}$ that satisfy the following compatibility condition.

For each cone $\sigma$ of $\Delta$, there is a decomposition $E = \oplus_{[u] \in M_{\sigma}} E_{[u]}$ such that 
$$E^{\rho}(i) = \sum_{[u](v_{\rho}) \ge i} E_{[u]}$$
for every ray $\rho$ contained in $\sigma$ and $i \in \mathbb{Z}$. 
\end{theorem}

A morphism in this category is a map $E \to F$ that takes $E^{\rho}(i)$ into $F^{\rho}(i)$. 

We discuss the functor from toric vector bundles to vector spaces with filtrations. Given a toric vector bundle $\mathcal{V}$, the vector space $E$ is the fiber over the identity of the torus. On any affine toric variety, any toric vector bundle splits as a direct sum of toric line bundles, and the toric line bundles are indexed by classes $[u] \in M_{\sigma}$. The decomposition $E = \oplus_{[u] \in M_{\sigma}} E_{[u]}$ is induced by the decomposition of $\mathcal{V}$ restricted to $U_{\sigma}$. If $\sigma$ is maximal, then the Chern class of $\mathcal{V}|_{U_{\sigma}}$ is given by $\prod (1 - x^{u})^{\dim E_u}$. 

If $\mathcal{V}$ is a line bundle with divisor $a_1 D_1 + \dotsb + a_k D_k$, then, identifying the fiber over the identity with $k$, the filtrations are given by 
$$E^{\rho_i}(j) = \begin{cases} k &  j \le a_i \\ 0 & j > a_i \end{cases}.$$

The filtrations associated to $\oplus \pi_i^* \mathcal{O}(1)$ are therefore given by
$$F^i(j) = \begin{cases} k^E & j \le 0 \\
0 & j \ge 0 \end{cases},$$
$$F^S(j) = \begin{cases} k^E & j \le 0 \\
k^{E \setminus S} & j = 1 \\
0 & j > 1 \end{cases}.$$

The filtrations associated to $\mathcal{S}_L$ are the induced filtrations of the subspace $L \subseteq k^E$, and the filtrations associated to $\mathcal{Q}_L$ are the induced filtrations on $k^E/L$. 

Recall Klyachko's complex. 

\begin{theorem}\cite{Klaychko}*{Theorem 4.1.1}

\end{theorem}




\begin{proposition}\label{prop:augmentedcohomology}
We have that $h^0(X_{St_n}, \mathcal{S}_L) = rk(M) + \#\{\text{coloops of }M\}$, $h^0(X_{St_n}, \mathcal{Q}_L) = 2n - rk(M) - \#\{\text{coloops of }M\}$, and all higher cohomology of $\mathcal{S}_L$ and $\mathcal{Q}_L$ vanishes.
\end{proposition}

\begin{corollary}
The $H$-action on $\Gamma(X_{St_n}, \mathcal{Q}_L)$ has an open dense orbit.
\end{corollary}

\begin{proof}
The $H$-action on $\Gamma(X_{St_n}, \oplus \pi_i^* \mathcal{O}(1))$ has an open dense orbit, and Proposition~\ref{prop:augmentedcohomology} implies that the map $\Gamma(X_{St_n}, \oplus \pi_i^* \mathcal{O}(1)) \to \Gamma(X_{St_n}, \mathcal{Q}_L)$ is surjective. 
\end{proof}

\begin{corollary}
For an arbitrary matroid $M$, the Euler characteristic of the augmented tautological $K$-class is $rk(M) + \#\{\text{coloops of }M\}$ for the subbundle, and $2n - rk(M) - \#\{\text{coloops of }M\}$ for the quotient bundle.
\end{corollary}

\begin{proof}
Follows from valuativity.
\end{proof}

To prove Proposition~\ref{prop:augmentedcohomology}, we use Klyachko's formula for the cohomology of a toric vector bundle in terms of the filtration data associated to it. See section 4 on Klyachko's paper ``Equivariant bundles on toral varieties.'' We recall how this works.

Fix a character $\chi$ and a vector bundle $\mathcal{V}$ on a smooth proper toric variety (Klyachko assumes smoothness, but I think it's probably not necessary). Suppose that $\chi$ corresponds to a vector space $F$ and filtrations $\{F^{\rho}(i)\}$. We will give a perfect complex that computes the $\chi$-isotypic component of $H^i(X(\Delta), \mathcal{V})$. 

For a cone $\sigma$, set $F_{\sigma}(\chi) = F/\sum_{\rho \in \sigma(1)} F^{\rho}(\langle \chi, v_{\rho} \rangle)$. For the cone $\sigma = \{0\}$, we define $F_{\sigma}(\chi) = F$. Then there is a complex
$$C^{\bullet}(\mathcal{V}, \chi) \colon 0 \to F \to \bigoplus_{\dim \sigma = 1} F_{\sigma}(\chi) \to \bigoplus_{\dim \sigma = 2} F_{\sigma}(\chi) \to \dotsb \to \bigoplus_{\dim \sigma = n} F_{\sigma}(\chi) \to 0,$$
where the differentials are the natural maps (which requires choosing an orientation of every cone, to get signs to work out.)

Klyachko shows that $H^i(C^{\bullet}(\mathcal{V}, \chi)) = H^i(X(\Delta), \mathcal{V})_{\chi}$. 

Note that 
$$H^i(X_{St_n}, \oplus \pi_i^* \mathcal{O}(1)) = H^i(X_{St_n}, \oplus_{i=1}^{n} \pi_i^* \mathcal{O}(1)) = \bigoplus_{i=1}^{n} H^i(\mathbb{P}^1, \mathcal{O}(1)),$$
where the last equality uses the Kunneth formula and the fact that $X_{St_n} \to (\mathbb{P}^1)^n$ is $\mathcal{O}$-connected. In particular, we see that the claimed formula holds for $\oplus \pi_i^* \mathcal{O}(1)$. 
By the short exact sequence $0 \to \mathcal{S}_L \to \oplus \pi_i^* \mathcal{O}(1) \to \mathcal{Q}_L \to 0$, we see that $h^i(X_{St_n}, \mathcal{Q}_L) = h^{i + 1}(X_{St_n}, \mathcal{S}_L)$ for $i > 0$. 

We first compute $\Gamma(X_{St_n}, \mathcal{S}_L)$ and the image of $\Gamma(X_{St_n}, \oplus \pi_i^* \mathcal{O}(1))$ in $\Gamma(X_{St_n}, \mathcal{Q}_L)$. First, note that 
$$\Gamma(X_{St_n}, \oplus \pi_i^* \mathcal{O}(1)) = \bigoplus_{i=1}^{n} \Gamma(X_{St_n}, \pi_i^* \mathcal{O}(1)) = \bigoplus_{i=1}^{n} k x_i \oplus k y_i,$$
where $x_i$ (resp. $y_i$) is a section of $\mathcal{O}(1)$ on $\mathbb{P}^1$ which restricts to the function $x$ (resp. $1$) on $\mathbb{P}^1$. 
To check if a global section of $\oplus \pi_i^* \mathcal{O}(1)$ vanishes in $\Gamma(X_{St_n}, \mathcal{Q}_L)$, it suffices to check if it vanishes when restricted to $\mathbb{A}^n$. Recalling that $\oplus \pi_i^* \mathcal{O}(1)|_{\mathbb{A}^n}$ has a natural trivialization, we see that a section $(a_1x_1, b_1y_1, a_2x_2, \ldots, a_nx_n, b_ny_n)$ of $\oplus \pi_i^* \mathcal{O}(1)$ vanishes in $\mathcal{Q}_L$ if and only if $(a_1x_1 + b_1, a_2x_2 + b_2, \ldots, a_nx_n + b_n)$ is contained in $L$ for all $(x_1, \ldots, x_n) \in \mathbb{A}^n$. 

We claim that $(a_1x_1 + b_1, a_2x_2 + b_2, \ldots, a_nx_n + b_n)$ is contained in $L$ for all $(x_1, \ldots, x_n)$ if and only if $(b_1, \ldots, b_n) \in L$, and for each $i$ with $a_i$ non-zero, $i$ is a coloop of $M$. The sufficiency of these conditions is clear.

If $(a_1x_1 + b_1, a_2x_2 + b_2, \ldots, a_nx_n + b_n)$ is contained in $L$ for all $(x_1, \ldots, x_n)$, then evaluating at $(0, \ldots, 0)$ we see that $(b_1, \ldots, b_n)$ is contained in $L$. Subtracting, we may assume that $b_1 = \dotsb = b_n = 0$. For a fixed coordinate $i$, we see by considering the set of point $(0, \ldots, p_i, \ldots, 0)$ for all $p_i \in k$ that if $a_i \not= 0$, then $i$ must be a coloop of $L$.

This implies that $h^0(X_{St_n}, \mathcal{S}_L) = rk(M) + \#\{\text{coloops of }M\}$, and that the dimension of the image of $\Gamma(X_{St_n}, \oplus \pi_i^* \mathcal{O}(1))$ in $\Gamma(X_{St_n}, \mathcal{Q}_L)$ is $2n - rk(M) - \#\{\text{coloops of }M\}$. Therefore, in order to show $h^1(X_{St_n}, \mathcal{S}_L) = 0$, it suffices to show that $h^0(X_{St_n}, \mathcal{Q}_L) = 2n - rk(M) - \#\{\text{coloops of }M\}$. Therefore, to prove the Proposition it suffices to compute $h^i(X_{St_n}, \mathcal{Q}_L)$ for all $i$. 

We now fix a character $\chi = (\chi_1, \ldots, \chi_n)$, and we will study the complex $C^{\bullet}(\mathcal{Q}_L, \chi)$. Note that 
$$F_{\rho_i}(\chi) = \begin{cases} 0, \chi_i \le 0\\  k^E/L,  \chi_i > 0\end{cases}, \quad F_{\rho_S}(\chi) = \begin{cases} 0, \langle \chi, v_{\rho_S} \rangle \le 0 \\ 
k^{S}/(L \cap k^S), \langle \chi, v_{\rho_S} \rangle = 1 \\
k^E/L, \langle \chi, v_{\rho_S} \rangle > 1\end{cases}.$$

\begin{lemma}\label{lem:pairing}
There is a collection of pairs of cones $(\tau, \sigma)$ satisfying the following properties.
\begin{enumerate}
\item Every cone $\tau$ with $F_{\tau}(\chi) \not= 0$ appears in a unique pair.
\item For every pair $(\tau, \sigma)$, $\tau$ is a facet of $\sigma$.
\item For a pair $(\tau, \sigma)$ with $\tau \not= \{0\}$, the ray $\rho$ of $\sigma$ that is not contained in $\tau$ has $\langle \chi, v_{\rho} \rangle \ge \langle \chi, v_{\rho'} \rangle$ for some ray $\rho'$ of $\tau$. 
\end{enumerate}
\end{lemma}

\begin{proof}
Let $P$ be the set of coordinates where $\chi_i > 0$. First we do the case when $P = E$. Then the only cones $\sigma$ with $F_{\sigma}(\chi) \not= 0$ are those that correspond to $I \le \emptyset$. Suppose $\chi_i \ge \chi_j$ for all $j$. Then, for $i \not \in I$, pair the cones $(I, I \cup \{i\})$.

Now suppose that $P$ is a proper subset of $E$. We only need to consider cones corresponding to $I \le \mathcal{F}$ for $I \subseteq P$. Let $F_i$ be the smallest element of $\mathcal{F}$ that is not contained in $P$ (recall that we view $\mathcal{F}$ as having maximal element $E$). If $F_i \cap P$ is not an element of $\mathcal{F}$, then we pair $I \le \mathcal{F}$ with $I \le \mathcal{F} \cup F_i \cap P$. Note that these are indeed cones of $\Sigma_{St_n}$, as $I \subseteq P$.
\end{proof}


We now prove Proposition~\ref{prop:augmentedcohomology}. Let $L_i = \bigoplus_{\tau} F_{\tau}(\chi)$, where $\tau$ is a dimension $i$ cone appearing as the smaller-dimensional cone of a pair in the output of Lemma~\ref{lem:pairing}, and let $U_i = \bigoplus_{\sigma} F_{\sigma}(\chi)$ be the dimension $i + 1$-cones that are large-dimensional elements of their pair. Then we can rewrite $C^{\bullet}(\mathcal{Q}_L, \chi)$ as
$$0 \to L_0 \to L_1 \oplus U_0 \to L_2 \oplus U_1 \to L_3 \oplus U_2 \to \dotsb.$$
Note that $L_0 \twoheadrightarrow U_0$, and $L_i \to U_i$ is an isomorphism for $i > 0$. Let $f_i$ be the map $L_i \to L_{i+1}$. Note that the map $U_i \to U_{i+1}$ can be identified with $-f_{i+1}$. 

We now check exactness away from degree $0$. Let $(a, b) \in L_{i + 1} \oplus U_i$. As the projection onto $U_{i+1}$ of $d(a,b)$ is $a - f_{i}(b)$, we see that if $d(a,b) = 0$, then $(a, b) = d(b, 0)$, as desired. \matt{This can probably be phrased as ``here's a nullhomotopy,'' although I don't see how.}

We now compute the cohomology in degree $0$, which is the kernel of the natural map $k^E/L \to \oplus_{\rho} F_{\rho}(\chi)$. If the $i$th coordinate of $\chi$ is positive, then $F_{\rho_i}(\chi) = k^E/L$ and thus $H^0(X_{St_n}, \mathcal{Q}_L)_{\chi} = 0$. If the $i$th and $j$th coordinate of $\chi$ are both negative, then $F_{\rho_{E \setminus \{i, j\}}}(\chi) = k^E/L$, so $H^0(X_{St_n}, \mathcal{Q}_L)_{\chi} = 0$. 

If $\chi = 0$, then $F_{\rho}(\chi) = 0$ for all $\rho$, so $H^0(X_{St_n}, \mathcal{Q}_L)_{\chi} = k^E/L$. If $\chi = -e_i$, then $F_{\rho_{E \setminus i}}(\chi) = k^{E \setminus i}/(L \cap k^{E \setminus i})$, and every other $F_{\rho}(\chi)$ is contained in $k^{E \setminus i}/(L \cap k^{E \setminus i})$. If $i$ not a coloop of $M$, then we see that $k^{E}/L \stackrel{\sim}{\to} k^{E \setminus i}/(L \cap k^{E \setminus i})$, so $H^0(X_{St_n}, \mathcal{Q}_L)_{\chi} = 0$. If $i$ is a coloop, we see that the kernel is $1$-dimensional, so $\dim H^0(X_{St_n}, \mathcal{Q}_L)_{\chi} = 1$. Putting this all together gives the result. 

\begin{proposition}\label{prop:twistedcohomology}
We have that $h^0(X_{St_n}, \mathcal{Q}_L(-X_{A_{n-1}})) = n - rk(M)$, and $h^i(X_{St_n}, \mathcal{Q}_L(-X_{A_{n-1}})) = 0$ for $i > 0$.
\end{proposition}

Note that (need reference) the filtrations defining $\mathcal{Q}_L(-X_{A{-n-1}})$ are the same for $\rho \not= \rho_{\emptyset}$, and 
$$F^{\rho_{\emptyset}}(i) = \begin{cases} k^E/L, i \le 0 \\ 0, i > 0 \end{cases}.$$

\begin{proof}
The same argument as in Proposition~\ref{prop:augmentedcohomology} shows that all the higher cohomology vanishes, and that $H^0(X_{St_n}, \mathcal{Q}_L(-X_{A_{n-1}}))_{\chi} = 0$ unless $\chi = 0$.
\end{proof}

\begin{proposition}
We have that $h^0(X_{A_{n-1}}, \underline{\mathcal{S}_L}) = \#\{\text{coloops of }M\}$, $h^0(X_{A_{n-1}}, \underline{\mathcal{Q}_L}) = n - \#\{\text{coloops of }M\}$, and all higher cohomology of $\underline{\mathcal{S}_L}$ and $\underline{\mathcal{Q}_L}$ vanishes.
\end{proposition}

\begin{proof}
By the exact sequence $$0 \to \underline{\mathcal{S}_L} \to \underline{\mathbb{C}}^{n}_{inv} \to \underline{\mathcal{Q}_L} \to 0,$$
we see that $h^i(X_{A_{n-1}}, \underline{\mathcal{Q}_L}) = h^{i + 1}(X_{A_{n-1}}, \underline{\mathcal{S}_L})$ for $i > 0$. By a similar computation as in the proof of Proposition~\ref{prop:augmentedcohomology}, we see that $h^0(X_{A_{n-1}}, \underline{\mathcal{S}_L}) = \#\{\text{coloops of }M\}$, and the image of $\Gamma(X_{A_{n-1}}, \underline{\mathbb{C}}^{n}_{inv})$ in $\Gamma(X_{A_{n-1}}, \underline{\mathcal{Q}_L})$ has dimension $n - \#\{\text{coloops of }M\}$. 
Using the exact sequence
$$0 \to \mathcal{Q}_L(-X_{A_{n-1}}) \to \mathcal{Q}_L \to \mathcal{Q}_L|_{X_{A_{n-1}}} \to 0,$$
the identification of $\mathcal{Q}_L|_{X_{A_{n-1}}}$ with $\underline{\mathcal{Q}_L}$, and Proposition~\ref{prop:augmentedcohomology} and \ref{prop:twistedcohomology} gives the desired the dimension of the cohomology groups of $\underline{\mathcal{Q}_L}$. We see that $\Gamma(X_{A_{n-1}}, \underline{\mathbb{C}}^{n}_{inv}) \to \Gamma(X_{A_{n-1}}, \underline{\mathcal{Q}_L})$ is surjective, so $h^1(X_{A_{n-1}}, \underline{\mathcal{S}_L}) = 0$. 
\end{proof}


\section{Other results on cohomology}

\begin{proposition}
Let $k$ be a field of characteristic zero, and let $L \subseteq k^n$. Then
\begin{enumerate}
\item $H^i(W_L, \wedge^j \mathcal{S}_L^{\vee}) = 0$ for all $i \ge 0, j > 0$, 
\item $H^i(\underline{W}_L, \wedge^j \underline{\mathcal{S}}_L^{\vee}) = 0$ for all $i > 0, j > 0$, and 
\item $\oplus_{j}H^0(\underline{W}_L, \wedge^j \underline{\mathcal{S}}_L^{\vee}) \stackrel{\sim}{\to} OS^*_L$. 
\end{enumerate}
\end{proposition}

\begin{proof}
Note that the mixed Hodge numbers of $L$ and $T \cap \mathbb{P}L$ are zero except for $h^{p, 0}$. For $L$ this is obvious, for $T \cap \mathbb{P}L$ it follows from the fact that the map $H^*(T) \to H^*(T \cap \mathbb{P}L)$ is surjective. The degeneration of the logarithmic Hodge-de Rham spectral sequence then implies (i). 

Note that $\mathcal{S}_L|_{W_L} = \mathcal{O}_{W_L} \oplus T_{\underline{W}_L/k}(- \log D)$ because we are in characteristic zero. This implies (ii). For (iii), mixed Hodge theory implies that $\oplus_j H^0(\underline{W}_L, \wedge^j T_{\underline{W}_L/k}(- \log D)) \stackrel{\sim}{\to} \overline{OS}^*_L$. We see that 
$$\oplus_{j}H^0(\underline{W}_L, \wedge^j \underline{\mathcal{S}}_L^{\vee}) \stackrel{\sim}{\to} \overline{OS}^*_L \otimes \bigwedge k \stackrel{\sim}{\to} OS^*_L.$$
\end{proof}

\begin{proposition}
The locus in $Hilb_{X_{A_n}}$ of wonderful varieties for a matroid $M$ is open, and is isomorphic to $Gr_M$. 
\end{proposition}

\begin{proof}
Fix a realization $L$. Note that an open neighborhood of $[\underline{W}_L]$ in $Gr_M$ consists of geometrically integral varieties, and hence all the geometric fibers are wonderful varieties. Then it suffices to show the family of wonderful varieties over $Gr_M$ is flat. If $Gr_M$ is reduced, then this follows from the fact that the Hilbert polynomial is constant. Otherwise, it should follows from a blow-up computation. 
\end{proof}

\begin{corollary}
The cohomology $H^0(\underline{W}_L, \underline{\mathcal{Q}}_L)$ is not determined by the matroid of $M$, and $H^1(\underline{W}_L, \underline{\mathcal{Q}}_L)$ is non-zero for some matroids. 
\end{corollary}

\begin{proof}
We can identify $H^0(\underline{W}_L, \underline{\mathcal{Q}}_L)$ with the tangent space of $[\underline{W}_L]$ in $Hilb_{X_{A_n}}$, which can identifies with the tangent space of $[L]$ in $Gr_M$. This is not combinatorially determined by $Gr_M$ can be singular. The possible non-vanishing of $H^1(\underline{W}_L, \underline{\mathcal{Q}}_L)$ follows from the fact the tangent-obstruction theory. 
\end{proof}


\end{document}